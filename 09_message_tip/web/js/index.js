
window.onload = function(){
    let vm = new Vue({
        el: '#app',
        data: {
            input: "",
            messages: JSON.parse(localStorage.getItem('app-messgaes')) || []
        },
        methods: {
            sendMessage(){
                this.messages.push({
                   content: this.input,
                   time: moment().locale('zh-cn').format('L')
                })
                localStorage.setItem('app-messgaes',JSON.stringify(this.messages) || [])
                this.input = '';
            }
        }
    })

}