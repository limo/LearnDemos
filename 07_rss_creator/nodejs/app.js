const xml2js = require('xml2js');
const fs = require('fs');
const path = require('path');

let rssData = {
    rss: {
        $: {
            version:'2.0'
        },
        channel: {
            title: '百度首页',
            link: 'https://www.baidu.com/',
            description: '百度一下',
            item: [
                {
                    title: '百度学术',
                    link: 'http://xueshu.baidu.com/',
                    description: '百度学术'
                },
                {
                    title: '百度地图',
                    link: 'https://map.baidu.com/',
                    description: '百度地图'
                }
            ]
        }
    }
};

let builder = new xml2js.Builder();
let xml = builder.buildObject(rssData);
fs.writeFileSync(path.join(__dirname,'atom.xml'),xml);