```bash
npm install
npm start
```

产生 atom.xml

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<rss version="2.0">
  <channel>
    <title>百度首页</title>
    <link>https://www.baidu.com/</link>
    <description>百度一下</description>
    <item>
      <title>百度学术</title>
      <link>http://xueshu.baidu.com/</link>
      <description>百度学术</description>
    </item>
    <item>
      <title>百度地图</title>
      <link>https://map.baidu.com/</link>
      <description>百度地图</description>
    </item>
  </channel>
</rss>
```