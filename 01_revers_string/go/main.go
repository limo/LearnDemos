package main 

import "fmt"
import "os"

func reverse(s string) string {
	runes := []rune(s)
	for from, to := 0, len(runes)-1;from < to;from, to = from + 1, to - 1 {
		runes[from], runes[to] = runes[to], runes[from]
	}
	return string(runes)
}

func main(){
	args := os.Args
	inputStr := args[1]
	outputStr := reverse(inputStr)
	fmt.Println(outputStr)
}