const {remote,ipcRenderer} = require('electron')

const {Menu, MenuItem} = remote

let vm = null;
window.onload = function(){
    vm = new Vue({
        el:'#app',
        data:{
            input: ''
        },
        
        methods: {
            // update: _.debounce(function(e) {
            //   const 
            //   this.input = e.target.value
            // }, 300)
        }
    })
}

ipcRenderer.on('OPEN_FILE_DATA',(event,data)=>{
    console.log('OPEN_FILE_DATA:'+data);
    vm.$data.input = data;
})

ipcRenderer.on('SAVE_FILE_DATA',(event,data)=>{
    ipcRenderer.send('SAVE_TO_FILE',vm.$data.input)
})