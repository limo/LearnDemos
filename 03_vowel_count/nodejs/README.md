```bash
$ node app banana
{ a: 3, e: 0, i: 0, o: 0, u: 0 }

$ node app use
{ a: 0, e: 1, i: 0, o: 0, u: 1 }
```