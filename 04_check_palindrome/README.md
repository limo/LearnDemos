## 判断是否为回文

> 判断用户输入的字符串是否为回文。回文是指正反拼写形式都是一样的词

- [nodejs实现](/04_check_palindrome/nodejs)   